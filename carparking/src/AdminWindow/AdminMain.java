package AdminWindow;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class AdminMain extends JFrame{

    private JLabel inputImage;
    private JLabel outputImage;
    private JLabel label1;
    private JLabel label2;
    private InOutPanel inout;//������ ������ � ������
    private JButton dataBase;//������ ��� ������ � ��

    public AdminMain(){

        this.setSize(800,600);
        this.setResizable(false);
        this.setLayout(null);
        setVisible(true);

        inout = new InOutPanel(this);
        this.add(inout);

        dataBase = new JButton("���� ������");
        dataBase.setBounds(550,425,130,60);
        this.add(dataBase);

        inputImage = new JLabel();
        setInputImage(0,null);
        this.add(inputImage);

        outputImage = new JLabel();
        setOutputImage(0,null);
        this.add(outputImage);

        label1 = new JLabel();
        label2 = new JLabel();
        setResultArea(0,null);

        //���� ������ �������� ����
        JLabel sumLb = new JLabel("���� � ���");
        sumLb.setBounds(10,400,300,15);
        this.add(sumLb);

        JTextArea sumTx = new JTextArea();
        sumTx.setBounds(10,415,200,20);
        this.add(sumTx);

        JButton sumBt = new JButton("��������");
        sumBt.setBounds(210,415,100,20);
        this.add(sumBt);

        //���� ������ �������� ������
        JLabel discLb = new JLabel("������");
        discLb.setBounds(10,440,300,15);
        this.add(discLb);

        JTextArea discTx = new JTextArea();
        discTx.setBounds(10,455,200,20);
        this.add(discTx);

        JButton discBt = new JButton("��������");
        discBt.setBounds(210,455,100,20);
        this.add(discBt);

        //���� ������ �������� �������
        JLabel timeLb = new JLabel("����� ����������");
        timeLb.setBounds(10,480,300,15);
        this.add(timeLb);

        JTextArea timeTx = new JTextArea();
        timeTx.setBounds(10,495,200,20);
        this.add(timeTx);

        JButton timeBt = new JButton("��������");
        timeBt.setBounds(210,495,100,20);
        this.add(timeBt);


        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.repaint();

    }
    //��������� �������� �� ����
    public void setInputImage(int mode,String filepath){

        inputImage.setBounds(10,10,480,360);
        Image buff = null;
        ImageIcon img;
        Image dimg = null;

        switch (mode){
            //��������� �������� ��� ������� ���������
            case 0 :
                try {
                    buff = ImageIO.read(new File(getClass().getResource("default.png").getPath().substring(1)));
                } catch (IOException e) {
                    System.out.println("Reading file error");
                }
                dimg = buff.getScaledInstance(inputImage.getWidth(), inputImage.getHeight(), Image.SCALE_SMOOTH);
                img = new ImageIcon(dimg);
                inputImage.setIcon(img);
                break;
            //���� � ������
            case 1:
                try {
                    buff = ImageIO.read(new File(filepath.substring(1)));
                } catch (IOException e) {
                    System.out.println("Reading file error");
                }
                dimg = buff.getScaledInstance(inputImage.getWidth(), inputImage.getHeight(), Image.SCALE_SMOOTH);
                img = new ImageIcon(dimg);
                inputImage.setIcon(img);
                break;
             //�������� ��� ������ ������
            case 2:
                try {
                buff = ImageIO.read(new File(getClass().getResource("error.png").getPath().substring(1)));
            } catch (IOException e) {
                System.out.println("Reading file error");
            }
                dimg = buff.getScaledInstance(inputImage.getWidth(), inputImage.getHeight(), Image.SCALE_SMOOTH);
                img = new ImageIcon(dimg);
                inputImage.setIcon(img);
                break;
            default:
                System.out.println("Incorrect image setting mode.");
        }
    }
    //��������� �������� �� �����
    public void setOutputImage(int mode, String directory){
        outputImage.setBounds(500, 270, 280,100);
        ImageIcon img;
        Image buff = null;
        Image dimg = null;
        String filepath = directory + "/" + "croppedPlateBin.png";

        switch (mode){
            //��������� ��������
            case 0:
                outputImage.setIcon(null);
                break;
            //��������� ����������
            case 1:
                try {
                    buff = ImageIO.read(new File(filepath.substring(1)));
                } catch (IOException e) {
                    System.out.println("Reading file error");
                }
                dimg = buff.getScaledInstance(outputImage.getWidth(), outputImage.getHeight(), Image.SCALE_SMOOTH);
                img = new ImageIcon(dimg);
                outputImage.setIcon(img);
                break;
            //������(��� ������)
            case 2:
                try {
                    buff = ImageIO.read(new File(getClass().getResource("errorSmall.png").getPath().substring(1)));
                } catch (IOException e) {
                    System.out.println("Reading file error");
                }
                dimg = buff.getScaledInstance(outputImage.getWidth(), outputImage.getHeight(), Image.SCALE_SMOOTH);
                img = new ImageIcon(dimg);
                outputImage.setIcon(img);
                break;
            default:
                System.out.println("Incorrect image setting mode.");
        }

    }
    //��������� ������ ������
    public void setResultArea(int mode,String plate){


        switch (mode){
            case 0:
                label1.setText("�����������");
                label1.setFont(new Font("Colibri", Font.PLAIN,16));
                label1.setBounds(510, 20, 200, 50);
                this.add(label1,BorderLayout.NORTH);
                break;
            case 1:
                label1.setText("��������� �����:");
                label1.setFont(new Font("Colibri", Font.PLAIN,16));
                label1.setBounds(510, 20, 200, 50);
                this.add(label1);

                label2.setText(plate);
                label2.setFont(new Font("Colibri", Font.PLAIN,24));
                label2.setBounds(510,100,200,50);
                this.add(label2);
                break;
            case 2:
                label1.setText("�� ������� ���������� �����");
                label1.setFont(new Font("Colibri", Font.PLAIN,16));
                label1.setBounds(510, 20, 300, 50);
                this.add(label1);

                label2.setText("");
                label2.setBounds(510,100,200,50);
                this.add(label2);
                break;
            case 3:
                label1.setText("����� �� ������");
                label1.setFont(new Font("Colibri", Font.PLAIN,16));
                label1.setBounds(510, 20, 200, 50);
                this.add(label1);

                label2.setText("");
                label2.setBounds(510,100,200,50);
                this.add(label2);
                break;
            case 4:
                label1.setText("����������� ����");
                label1.setFont(new Font("Colibri", Font.PLAIN,16));
                label1.setBounds(510, 20, 200, 50);
                this.add(label1);

                label2.setText("");
                label2.setBounds(510,100,200,50);
                this.add(label2);
                break;
            default:
                System.out.println("Incorrect result text setting mode.");
        }
    }
}
