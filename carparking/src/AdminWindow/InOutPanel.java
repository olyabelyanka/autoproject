package AdminWindow;

import auto_project_recognition.OCR;
import auto_project_recognition.OCREmpty;
import auto_project_recognition.OCRError;
import auto_project_recognition.OCRNoFile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InOutPanel extends JPanel {
    private JButton inButton;
    private JButton outButton;
    private static String carNumber;

    public InOutPanel(AdminMain frame){
        this.setLayout( new BorderLayout());
        this.setBounds(350, 400, 100, 100);

        inButton = new JButton("�����");
        outButton = new JButton("�����");

        this.add(inButton, BorderLayout.NORTH);
        this.add(outButton, BorderLayout.SOUTH);
        //���������� ������ ������
        inButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                OCR car = new OCR();
                boolean done = false;
                while(!done){

                    try{
                        carNumber=car.run();
                        frame.setInputImage(1,car.getFilePath());
                        frame.setOutputImage(1,car.getDirectory());
                        frame.setResultArea(1,carNumber);
                        done = true;
                    }
                    catch (OCRError e1){
                        frame.setOutputImage(1,car.getDirectory());
                        frame.setInputImage(1,car.getFilePath());
                        frame.setResultArea(2,null);
                        done = true;
                    }
                    catch(OCREmpty e2){
                        frame.setInputImage(1,car.getFilePath());
                        frame.setOutputImage(2,null);
                        frame.setResultArea(3,null);
                        done = true;
                    }
                    catch(OCRNoFile e3){
                        frame.setInputImage(2,null);
                        frame.setOutputImage(0,null);
                        frame.setResultArea(4,null);
                        done=true;
                    }
                }
            }
        });

        outButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OCR car = new OCR();
                boolean done = false;
                while(!done){

                    try{
                        carNumber=car.run();
                        frame.setInputImage(1,car.getFilePath());
                        frame.setOutputImage(1,car.getDirectory());
                        frame.setResultArea(1,carNumber);
                        done = true;
                    }
                    catch (OCRError e1){
                        frame.setOutputImage(1,car.getDirectory());
                        frame.setInputImage(1,car.getFilePath());
                        frame.setResultArea(2,null);
                        done = true;
                    }
                    catch(OCREmpty e2){
                        frame.setInputImage(1,car.getFilePath());
                        frame.setOutputImage(2,null);
                        frame.setResultArea(3,null);
                        done = true;
                    }
                    catch(OCRNoFile e3){
                        frame.setInputImage(2,null);
                        frame.setOutputImage(0,null);
                        frame.setResultArea(4,null);
                        done=true;
                    }
                }
            }
        });

    }
}
