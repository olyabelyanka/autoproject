package Database;

import java.util.ArrayList;

/**
 * Created by ������� on 07.12.2015.
 * ����� ������������� ����������, ���� ���������� ������ ���� �� ������� �����, �� ����� �� ������ �� �����������������
 * (� �� out_time = null ) ����� ����� ��������� ������ ���������� ������������� �������� �������� � ������� � �� �������
 * �������� �� ����������� ������ � ������, ��������� �������� �� ������ � ��
 */
public class ArrivedError extends Exception{
    public ArrivedError(String msg){
        super(msg);
    }
    public ArrivedError(){}
}
