package Database;

import java.sql.SQLException;

/**
 * Created by ������� on 09.11.2015.
 */
public class Departure extends MainCarParkingClass{


    public Departure(String carNumber){ //����� � ����� sumWithDiscount();,getCountCar(); ;
        this.carNumber = carNumber;
    }
    // ���������� out_time � trips �������� ������ ������ � �������
    public void updateOutTime()throws DepartureError{
        if (!isOldCar()){
            throw new DepartureError();
        }
        String queryUpdateTrips = "update trips set out_time=now() where out_time=0 and num_auto='" +
                this.carNumber + "';";
        insert(queryUpdateTrips);
    }
    //��������� ���� �� ������ �� ������ ������ (�������� �� ��� ����� ����� ��������)
    boolean isOldCar(){
        boolean oldCar = true;
        String query = "select * from trips where num_auto='" +
                this.carNumber + "' and in_time = 0;";
        try {
            query(query);
            while(result.next()){
                oldCar = false;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            closeAlL();
        }
        return oldCar;
    }



    //������ ����� ���� �������
    private int getSumTime(){
        String updateSumTime = "update car SET sum_time=sum_time+(SELECT TIMESTAMPDIFF(HOUR, in_time, out_time)"
                + "FROM trips WHERE id=(SELECT MAX(id) FROM trips where num_auto='"
                +carNumber +"')) where num_auto='"+ carNumber +"'; ";
        String getSumTime = "select sum_time from car where num_auto='" +
                carNumber + "';";
        insert(updateSumTime);
        query(getSumTime);
        int sumTime=0;
        try {
            while(result.next()){
                sumTime = result.getInt(1);
                System.out.println("stime" + sumTime);
            }
        }catch(SQLException sqlEx) {
            sqlEx.printStackTrace();
        }finally{closeAlL();}
        return sumTime;
    }
    //������ ����� �������
    private int getParkingTime(){
        int parkingTime = 0;
        String updateParkingTime = "update car SET last_stand_time=(SELECT TIMESTAMPDIFF(HOUR, in_time, out_time)"
                + "FROM trips WHERE id=(SELECT MAX(id) FROM trips where num_auto='" + carNumber +"')) where num_auto='"
                + carNumber +"'; ";
        String getParkingTime = "select last_stand_time from car where num_auto='" +
                carNumber + "';";
        insert(updateParkingTime);
        query(getParkingTime);
        try {
            while(result.next()){
                parkingTime = result.getInt("last_stand_time");
                //System.out.println("ptime" + parkingTime);
            }
        }catch(SQLException sqlEx) {
            sqlEx.printStackTrace();
        }finally{closeAlL();}
        return parkingTime;
    }
    //�������� ����������� ������ � ������� car
    private void setDiscount(int disc){
        String updateDiscount = "update car SET sale= "+ disc +" where num_auto = '" + this.carNumber +"'";
        insert(updateDiscount);
    }
    public int getDiscount(){
        int disc  = 0;
        String getDiscount = "select sale from car where num_auto='" +
        this.carNumber + "';";
        query(getDiscount);
        try {
            while(result.next()){
                disc = result.getInt("sale");
            }
        }catch(SQLException sqlEx) {
            sqlEx.printStackTrace();
        }finally{closeAlL();}
        return disc;
    }
    public double sumWithDiscount(){
        int time = getParkingTime();
        double prise=0;
        int discount =0;               //������ � ���������
        prise = time*SumInHour;
        if(prise == 0){return prise;}
        else{
            int sumtime = getSumTime();
            discount = (sumtime/TimeForDiscount)*DiscountPersent;
            discount = (discount >= 50) ? 50 : discount;
            setDiscount(discount);
            prise = (prise*(100-discount))/100;
        }
        return prise;
    }

}
