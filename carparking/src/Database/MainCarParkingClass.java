package Database;

import java.sql.*;
import java.sql.SQLException;
/**
 * Created by ������� on 09.11.2015.
 */
public abstract class MainCarParkingClass {
    protected String carNumber = null;
    protected double SumInHour = 100; //����� � ��� � ������
    protected int DiscountPersent = 3; // ������ � ��������� �� ������ TimeForDiscount ���� ��������
    protected int TimeForDiscount = 2; // ����� �����, ������� �������� ������ � �����

    protected static Connection connection = null;
    protected static Statement statement = null;
    protected static ResultSet result;

    protected String url = "jdbc:mysql://localhost/";
    protected final String dbName = "cparking";
    protected final String driverName = "com.mysql.jdbc.Driver";
    protected String userName = "root";
    protected String password = "O2r3l4o5v6-1995";

    //��������� ������ � 3 ����
    public void setDiscountPersent(int DiscountPersent){
        this.DiscountPersent = DiscountPersent;
    }
    //��������� ���� � ���
    public void setSumInHour(double sum){
        this.SumInHour = sum;
    }
    //������ ������
    public int getDiscountPersent(){
        return this.DiscountPersent;
    }
    //������ ���� � ���
    public double setSumInHour(){
        return this.SumInHour;
    }
    //��������� �������
    public void setTimeForDiscount(int t){
        this.TimeForDiscount = t;
    }
    //������ �����
    public int getTimeForDiscount(){
        return this.TimeForDiscount;
    }

    // ������� ����� ����� �� �������
    public int getCountCar(){
        int countCars = 0;
        query("select * from trips where out_time=0;");
        try {
            while(result.next()){
                countCars++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            closeAlL();
        }
        return countCars;
    }

    //��������� ���������� ������ connection
    protected void closeAlL () {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setData(String url, String userName, String password) {
        this.url = url;
        this.password = password;
        this.userName = userName;
    }

    public final String getData() {
        String str = "url: " + this.url + "\n" +
                "database name: " + this.dbName +
                "\ndriver name: " + this.driverName +
                "\nuser name: " + this.userName +
                "\npassword: " + this.password;
        return str;
    }

    protected final void getConnection() {

        try {
            Class.forName(driverName).newInstance();
            connection = DriverManager.getConnection(url + dbName, userName, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //��������� ���������� �� �����

    //��������� ���-� � ��
    public void insert(String query) {                                          // �������� �� ����������� closeAll()
        getConnection();
        System.out.println("insert");

        try {
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            closeAlL();
        }
    }
    //������ �� ��������� ���������� �������

    //������
    public void query(String query){
        getConnection();
        System.out.println("query");
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
