package auto_project_recognition;
import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.*;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import javax.swing.filechooser.FileNameExtensionFilter;

public class OCR {
	
	private String finalNomer="";
	private String directory="";
	private String fileName = "";
	
	public String run() throws OCRError, OCREmpty,OCRNoFile{
	    System.out.println("\nRunning test detection");

	    // Create a plate detector from the cascade file in the resources
		CascadeClassifier plateDetector = new CascadeClassifier(getClass().getResource("haarcascade_russian_plate_number.xml").getPath().substring(1));

		//Opening plate image
		try {
			fileName = "photo/" + getPhotoFile();
		}
		catch(OCRNoFile e){
			System.out.println("No files found.");
			throw e;
		}

		Mat image = Imgcodecs.imread(getClass().getResource(fileName).getPath().substring(1));
		System.out.println(getClass().getResource(fileName).getPath().substring(1));

		//Creating archive directory, moving file
		createDirectory();
		moveFile();

	    // Detect plates in the image.
	    // MatOfRect is a special container class for Rect.
	    MatOfRect plateDetections = new MatOfRect();
	    plateDetector.detectMultiScale(image, plateDetections);
		//Error if no plates
		if(plateDetections.toArray().length==0){
			System.out.println("No plates detected.");
			throw new OCREmpty("No plates detected");
		}
	    System.out.println(String.format("Detected %s plates", plateDetections.toArray().length));

	     Rect rectCrop = null;
	    double compareWidth=0.0;
	    
	    // Draw a bounding box around each plate.
	    for (Rect rect : plateDetections.toArray()) {
	    	Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
	    	if(compareWidth<rect.width){
	    		compareWidth=rect.width;
	    		rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
	    	}
	    }

	    // Save the visualized detection.
	    System.out.println("Writing plateDetection.png");
	    Imgcodecs.imwrite("plateDetection.png", image);
	    Mat imageRoi = new Mat(image, rectCrop);
	    //Imgcodecs.imwrite(directory.substring(1)+"/croppedPlate.png",imageRoi);
	    
	    //Image processing
	    Imgproc.cvtColor(imageRoi,imageRoi,Imgproc.COLOR_RGB2GRAY);
	    System.out.println("Writing croppedPlate.png");
	    Imgcodecs.imwrite(directory.substring(1)+"/croppedPlate.png",imageRoi);
	    Size s = new Size(3,3);
	    Imgproc.GaussianBlur(imageRoi, imageRoi, s, 1, 1);//blurring
	    //Imgproc.equalizeHist(imageRoi, imageRoi);
	    s=new Size(400,133);
	    Mat ims = new Mat();
	    Imgproc.resize(imageRoi, ims, s);//standard size
		Imgproc.resize(imageRoi, imageRoi, s);
	    
	    //Thresholding
	    //Core.normalize(imageRoi, imageRoi, 0, 255, Core.NORM_MINMAX, CvType.CV_8U);
	    //Imgproc.threshold(imageRoi, imageRoi, 200, 255, Imgproc.THRESH_BINARY+Imgproc.THRESH_OTSU);
	    Imgproc.adaptiveThreshold(ims, ims, 255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 55, 28);
	    System.out.println("Writing croppedPlatBin.png");
		Imgcodecs.imwrite(directory.substring(1)+"/croppedPlateBin.png",ims);

	    //Finding contours
	    ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	    Mat hierarchy = new Mat();
	    Mat tmp = ims.clone();

	    Imgproc.findContours(tmp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
	    System.out.println(String.format("Detected %s contours", contours.size()));
	    
	    //���������� �� �������� ��������
	    double minHeight=s.height/5;
	    double maxHeight=s.height/2;
	    double minWidth=s.width/20;
	    double maxWidth=s.width/10;
	    
	    ArrayList<Mat> symbols = new ArrayList<Mat>();//������ ����������� �������� �� ������
	    ArrayList<Rect> rectangles = new ArrayList<Rect>();
	    
	    //����� ��������������� ����������� ��������
	    for(MatOfPoint contour : contours){
	    	Rect rec = Imgproc.boundingRect(contour);
	    	if(rec.height<=maxHeight && rec.height>=minHeight && rec.width<=maxWidth && rec.width>=minWidth){
	    		Imgproc.rectangle(imageRoi, new Point(rec.x, rec.y), new Point(rec.x + rec.width, rec.y + rec.height), new Scalar(0, 255, 0));
	    		rectangles.add(rec);
	    	}
	    }
		System.out.println("Writing croppedPlatBin.png");
	    Imgcodecs.imwrite(directory.substring(1)+"/croppedPlateBin.png",imageRoi);
	    
	    //���������� ��������������� �� �
	    int min,n;
	    for(int i=0;i<rectangles.size()-1;i++){
	    	min = rectangles.get(i).x;
	    	n=i;
	    	for(int k=i+1;k<rectangles.size();k++){
		    	if(min>rectangles.get(k).x){
		    		min=rectangles.get(k).x;	    		
		    		n=k;
		    	}
	    	}
	    	Collections.swap(rectangles,i,n);
	    }
	    
	    
	    //�������� ������� ��������
	    for(Rect rec: rectangles){
	    	tmp = new Mat(ims,rec);
	    	Imgproc.resize(tmp, tmp, new Size(20,30));
	    	Imgproc.adaptiveThreshold(tmp, tmp, 255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 51, 28);
    		symbols.add(tmp);
	    }
		//System.out.println(symbols.size());
		if(symbols.size()<8 || symbols.size()>9){
			System.out.println("Recognizing failed.");
			throw new OCRError("Recognizing failed.");
		}
		/*System.out.println("Creating symbols array.");
	    for(int i=0;i<symbols.size();i++){
	    	Imgcodecs.imwrite(String.format("symbol%d.png", i), symbols.get(i));
	    }*/
	    //Recognition
	    System.out.println("Recognizing...");
	    try{
			finalNomer = recognition(symbols);
		}
		catch(OCRError e){
			throw e;
		}

	    System.out.println("��������� �����:");
	    System.out.println(finalNomer);	  

		return finalNomer;
	}
	
	
	private String recognition(ArrayList<Mat> sym)throws OCRError{
		String s="";
		s+=recognLetter(sym.get(0));
		for(int i=1;i<4;i++){
			s+=recognNumber(sym.get(i));
		}
		s+=recognLetter(sym.get(4));
		s+=recognLetter(sym.get(5));
		
		for(int i=6;i<sym.size();i++){
			s+=recognNumber(sym.get(i));
		}
		if(s.contains("error")){
			throw new OCRError("Recognizing failed.");
		}
		else{
			return s;
		}
		
	}
	
	private String recognLetter(Mat letter){
		String[] lettersEx = {"A","B","E","K","M","H","O","P","C","T","Y","X"};
		String s="";
		double hamming,maxhamming=0;//��������������� ��������� ��������
		Mat letE = new Mat();
		Mat tmp = letter.clone();

		for(int i=0;i<lettersEx.length;i++){
			System.out.println(i);
			String path = getClass().getResource(String.format("symbols/letters/%d.png",i)).getPath().substring(1);
			letE = Imgcodecs.imread(path,Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
			//Imgcodecs.imwrite(String.format("symbol%d.png",i),letE);

			Core.bitwise_xor(letE, letter, tmp);
			hamming=1.0-(double)(Core.countNonZero(tmp))/(double)(Core.countNonZero(letE));
			if(maxhamming<hamming && hamming>0.5){
				s = lettersEx[i];
				maxhamming=hamming;
			}
			System.out.println(String.format("Hamming index = %f", hamming));
		}
		
		if(s==""){
			System.out.println(s);
			return "error";
		}
		else{
			System.out.println(s);
			return s;
		}
		
	}
	
	private String recognNumber(Mat number){
		
		String s="";
		double hamming,maxhamming=0;//��������������� ��������� ��������
		Mat letE = new Mat();
		Mat tmp = new Mat();
		
		for(int i=0;i<10;i++){
			System.out.println(i);
			String path = getClass().getResource(String.format("symbols/numbers/%d.png",i)).getPath().substring(1);
			letE = Imgcodecs.imread(path,Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
			//Imgcodecs.imwrite(String.format("symbol%d.png",i),letE);
            Core.bitwise_xor(letE, number, tmp);
			hamming=1.0-(double)(Core.countNonZero(tmp))/(double)(Core.countNonZero(letE));
			if(maxhamming<hamming && hamming>0.6){
				s = String.format("%d", i);
				maxhamming=hamming;
			}
			System.out.println(String.format("Hamming index = %f", hamming));
		}
		
		if(s==""){
			System.out.println(s);
			return "error";
		}
		else{
			System.out.println(s);
			return s;
		}
		
	}

	private String getPhotoFile()throws OCRNoFile{
		File directory = new File(getClass().getResource("photo").getPath());
		String[] files = directory.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".jpg");
			}
		});
		if(files.length==0){
			throw new OCRNoFile("No files found");

		}
		else return files[0];
	}

	private void createDirectory(){

		directory = this.getClass().getResource("archive").getPath();
		Calendar calendar = new GregorianCalendar();
		directory += "/" + calendar.get(Calendar.MONTH) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "/"
		+ calendar.get(Calendar.HOUR) + "_"+ calendar.get(Calendar.MINUTE)+ "_"+ calendar.get(Calendar.SECOND);
		System.out.println(directory);
	}

	private void moveFile(){
		File movdir = new File(directory);
		File defdir = new File(getClass().getResource(fileName).getPath());
		movdir.mkdirs();
		defdir.renameTo(new File(movdir,defdir.getName()));
	}

	public String getDirectory(){
		return directory;
	}

	public String getFilePath(){
		return directory + "/" + fileName.substring(6);
	}

	public String getNomer(){
		return finalNomer;
	}
}


