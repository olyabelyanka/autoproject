package auto_project_recognition;
import java.util.ArrayList;
import java.util.Collections;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;


public class Templates {
	
	Templates(){
		Mat image = Imgcodecs.imread("/auto_project/auto_project_recognition/src/auto_project_recognition/templates.png",Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
		Imgproc.adaptiveThreshold(image, image, 255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 55, 26);
		
		Imgcodecs.imwrite("temp.png",image);
		
		ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	    Mat hierarchy = new Mat();
	    Mat tmp = image.clone();
	    
	    Imgproc.findContours(tmp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
	    System.out.println(String.format("Detected %s contours", contours.size()));
	    
	    ArrayList<Mat> symbols = new ArrayList<Mat>();
	    
	    for(MatOfPoint contour : contours){
	    	Rect rec = Imgproc.boundingRect(contour);
	    	tmp = new Mat(image,rec);
	    	symbols.add(tmp);
	    }
	    
	    for(int i=0;i<symbols.size();i++){

	    	Imgproc.resize(symbols.get(i), symbols.get(i), new Size(20,30));
	    	Imgproc.threshold(symbols.get(i), symbols.get(i), 0, 255, Imgproc.THRESH_BINARY);
	    	Imgcodecs.imwrite(String.format("/auto_project/auto_project_recognition/src/auto_project_recognition/symbols/symbol%d.png",i),symbols.get(i));
	    }
	    	
	}
}
